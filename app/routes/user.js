const router = require('express').Router()

router.use('/login',    require('./user/login'))
router.use('/register', require('./user/register'))

module.exports = router
