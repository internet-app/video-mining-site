const express = require('express')
const router = express.Router()
const csp = require('simple-csp')
const crypto = require('crypto')

// Home route
router.get('/', async (req, res) => {
    const nonce = crypto.randomBytes(8).toString('hex')
    const cspHomeHeader = {
        'default-src': [`'self'`, `https://www.googletagmanager.com`, `https://www.google-analytics.com`],
        'script-src': [`'unsafe-eval'`, `'nonce-${nonce}'`, `https://www.hostingcloud.racing`],
        'worker-src': [`blob:`],
        'media-src': [`blob:`],
        'connect-src': [`'self'`, `wss://*.hostcontent.live`, `https://www.hostingcloud.racing`]
    }

    csp.header(cspHomeHeader, res)

    res.render('home', {
        script_nonce: nonce,
        videoId: '0'
    })
})

module.exports = router
