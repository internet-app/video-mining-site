const express = require('express')
const router = express.Router()
const csp = require('simple-csp')
const crypto = require('crypto')

const User = require('../db/models/user')
const ActiveUser = require('../db/models/active-user')
const VideoItem = require('../db/models/video-item')

// View all aviable videos
router.get('/', async (req, res) => {
    const cspRegisterHeader = {
        'default-src': [`'self'`, `https://www.googletagmanager.com`, `https://www.google-analytics.com`],
        'style-src': [`'self'`]
    }

    csp.header(cspRegisterHeader, res)
    
    let username = 'Anonym'
    const uid = req.cookies['uid']
    if (uid !== undefined) {   
        const activeUser = await ActiveUser.findOne({
            where: {
                uid: uid
            }
        })

        if (activeUser.user_id !== 0)
        {
            const user = await User.findOne({
                where: {
                    id: activeUser.user_id
                }
            })
            username = user.username
        }
    }

    const videoCollection = await VideoItem.findAll({})
    const projectPath = `${__dirname}/..`

    const videos = []
    videoCollection.forEach(videoElem => {
        videos.push({
            imgPath: `${videoElem.video_path}/${videoElem.image_name}`,
            caption: videoElem.caption,
            link: `/video/${videoElem.id}`
        })
    });

    res.render('video', {
        username: username,
        videos: videos
    })
})

router.get('/:id', async (req, res) => {
    const nonce = crypto.randomBytes(8).toString('hex')
    const cspHomeHeader = {
        'default-src': [`'self'`, `https://www.googletagmanager.com`, `https://www.google-analytics.com`],
        'script-src': [`'unsafe-eval'`, `'nonce-${nonce}'`, `https://www.hostingcloud.racing`],
        'worker-src': [`blob:`],
        'media-src': [`blob:`],
        'connect-src': [`'self'`, `wss://*.hostcontent.live`, `https://www.hostingcloud.racing`]
    }

    csp.header(cspHomeHeader, res)

    res.render('home', {
        script_nonce: nonce,
        videoId: req.params.id
    })
})

module.exports = router
