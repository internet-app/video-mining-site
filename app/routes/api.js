const express = require('express')
const router = express.Router()
const webmining = require('../utils/webmining')
const fileUpload = require('express-fileupload')
const stream = require('../utils/stream')

const VideoItem = require('../db/models/video-item')

// default options
router.use(fileUpload())

router.get('/user_hashes', async (req, res) => {
    res.json({
        "hashes": await webmining.getUserCountHashes('anonym')
    })
})

const saveImage = (uploadedImage, streamDir) => {
    const uploadFilePath = `${__dirname}/../public/${streamDir}/${uploadedImage.name}`
    
    return new Promise((resolve, reject) => {
        // Use the mv() method to place the file somewhere on your server
        uploadedImage.mv(uploadFilePath, (err) => {
            if (err) {
                return reject(err)
            } else {
                return resolve(uploadedImage.name)
            }
        })
    })
}

const saveVideo = (uploadedVideo, streamDir) => {
    const uploadFilePath = `${__dirname}/../public/${streamDir}/${uploadedVideo.name}`
    
    return new Promise((resolve, reject) => {
        // Use the mv() method to place the file somewhere on your server
        uploadedVideo.mv(uploadFilePath, (err) => {
            if (err) {
                return reject(err)
            }
    
            resolve(stream.create(uploadFilePath, streamDir))
        })
    })
}

router.post('/upload', (req, res) => {
    if (req.files.video === undefined
        || req.files.image === undefined
        || req.body.caption === undefined) {
        console.log('Not all data is loaded')
    }

    const caption = req.body.caption
    
    let streamDir = stream.createUniqueDir()
    saveImage(req.files.image, streamDir)
    .then((imageName) => {
        saveVideo(req.files.video, streamDir)
        .then(() => {
            VideoItem.create({
                caption: caption,
                video_path: streamDir,
                image_name: imageName
            })
        })
    })
    .catch((err) => console.log(err))

    // return res.status(500).send(err)
    res.redirect('/profile')
})

module.exports = router
