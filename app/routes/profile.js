const express = require('express')
const router = express.Router()
const csp = require('simple-csp')

const cspRegisterHeader = {
    'default-src': [`'self'`, `https://www.googletagmanager.com`, `https://www.google-analytics.com`],
    'style-src': [`'self'`]
}

const User = require('../db/models/user')
const ActiveUser = require('../db/models/active-user')

// Register Form
router.get('/', async (req, res) => {
    csp.header(cspRegisterHeader, res)
    
    let username = 'Anonym'
    const uid = req.cookies['uid']
    if (uid !== undefined) {   
        const activeUser = await ActiveUser.findOne({
            where: {
                uid: uid
            }
        })

        if (activeUser.user_id !== 0)
        {
            const user = await User.findOne({
                where: {
                    id: activeUser.user_id
                }
            })
            username = user.username
        }
    }

    res.render('profile', {
        username: username
    })
})

module.exports = router
