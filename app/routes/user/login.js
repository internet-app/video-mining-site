const bcrypt = require('bcrypt')
const express = require('express')
const router = express.Router()
const csp = require('simple-csp')

// Bring in User Model
const User = require('../../db/models/user')
const ActiveUser = require('../../db/models/active-user')

router.get('/', (req, res) => {
    const cspRegisterHeader = {
        'default-src': [`'self'`, `https://www.googletagmanager.com`, `https://www.google-analytics.com`]
    }
    
    csp.header(cspRegisterHeader, res)
    res.render('login')
})

router.post('/', async (req, res) => {
    req.checkBody('username', 'Username is invalid').matches('^(?=.{3,30}$)[a-zA-Z0-9]+$')
    req.checkBody('password', 'The password length must be greater than 7 and lower than 72').isLength({ min: 8, max: 71})

    // Check if validation of credentials is complete
    let errors = req.validationErrors()
    if (errors) {
        res.statusCode = 404
        res.setHeader('Content-Type', 'text/plain')
        res.send(errors)

        return
    }
    
    // Save user to database
    const userEntry = await User.findOne({ 
        where: {
            username: req.body.username
        }
    })

    if (userEntry === null) {
        res.redirect('/user/login')
        return
    }

    const isMatch = bcrypt.compare(req.body.password, userEntry.password)
    
    if (isMatch === false) {
        res.send('Fail')
        return
    }
    
    let uid = req.cookies['uid']
    if (uid === undefined) {
        uid = cryptoRandomString({length: 128, type: 'base64'})
        await ActiveUser.create({ 
            uid: uid,
            user_id: 0
        }).catch((err) => console.log(err))
    
       res.cookie('uid', uid, { maxAge: 900000, httpOnly: true })
    }

    await ActiveUser.update({
        uid: uid,
        user_id: userEntry.id
    }, {
        where: {
            uid: uid
        }
    }).catch((err) => console.log(err))

    res.redirect('/profile')
})

module.exports = router
