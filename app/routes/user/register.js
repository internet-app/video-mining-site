const bcrypt = require('bcrypt')
const express = require('express')
const router = express.Router()
const csp = require('simple-csp')

// Bring in User Model
const User = require('../../db/models/user')

const cryptRounds = 10

// Register Form
router.get('/', (req, res) => {
    const cspRegisterHeader = {
        'default-src': [`'self'`, `https://www.googletagmanager.com`, `https://www.google-analytics.com`]
    }
    
    csp.header(cspRegisterHeader, res)
    res.render('register')
})

router.post('/', async (req, res) => {
    req.checkBody('name', 'Name is invalid').matches('^(?=.{3,30}$)[a-zA-Z0-9]+$')
    req.checkBody('email', 'Email is invalid').matches('^(?=.{10,40}$)[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')
    req.checkBody('username', 'Username is invalid').matches('^(?=.{3,30}$)[a-zA-Z0-9]+$')
    req.checkBody('password', 'The password length must be greater than 7 and lower than 72').isLength({ min: 8, max: 71})
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password)
    
    // Check if validation of credentials is complete
    let errors = req.validationErrors()
    if (errors) {
        res.statusCode = 404
        res.setHeader('Content-Type', 'text/plain')
        const err = errors[0]
        res.send(`Error: ${err.msg} in the input field '${err.param}'`)
    } else {
        const userEntry = await User.findOne({ 
            where: {
                username: req.body.username
            }
        }).catch((err) => console.log(err))

        if (userEntry !== null) {
            res.send('Username is already exist')
            return
        }

        // Generate password hash with salt
        const hash = await bcrypt.genSalt(cryptRounds)
        .then((salt) => {
            return bcrypt.hash(req.body.password, salt)
        }).catch((err) => console.log(err))

        await User.create({
            name: req.body.name,
            email: req.body.email,
            username: req.body.username,
            password: hash
        }).catch((err) => console.log(err))

        res.redirect('/user/login')
    }
})

module.exports = router
