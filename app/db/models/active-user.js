const Sequelize = require('sequelize')
const dbPostgres = require('../postgres')

const ActiveUser = {
    uid:             { type: Sequelize.STRING(64) },
    user_id:         { type: Sequelize.INTEGER },
    
    // user fingerprinting
    user_agent:      { type: Sequelize.STRING(40) },
    // Make by country
    remote_address:  { type: Sequelize.INET       },
    accept_language: { type: Sequelize.STRING(40) },
    country:         { type: Sequelize.STRING     },
    city:            { type: Sequelize.STRING     }
}

module.exports = dbPostgres('active_user', ActiveUser)
