const Sequelize = require('sequelize')
const dbPostgres = require('../postgres')

const User = {
    name:       { type: Sequelize.STRING(200) },
    email:      { type: Sequelize.STRING(200) },
    username:   { type: Sequelize.STRING(200) },
    password:   { type: Sequelize.STRING(60) },
}

module.exports = dbPostgres('user', User)
