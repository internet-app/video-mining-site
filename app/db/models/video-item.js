const Sequelize = require('sequelize')
const dbPostgres = require('../postgres')

const VideoItem = {
    caption:    { type: Sequelize.STRING },
    video_path: { type: Sequelize.STRING },
    image_name: { type: Sequelize.STRING }
}

module.exports = dbPostgres('video_item', VideoItem)
