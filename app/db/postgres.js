const sequelize = require('sequelize')

const postgresCred = require('../config/postgres')

const db = new sequelize(postgresCred.DB, postgresCred.USER, postgresCred.PASSWORD , {
    dialect: 'postgres',
    host: postgresCred.HOST,
    logging: false
})

db.authenticate()
.then(() => {
  console.log('Connection to PostgresDB has been established successfully.')
})
.catch(err => {
  console.error('Unable to connect to the PostgresDB:', err)
})

module.exports = (modelName, model) => {
    const dbModel = db.define(modelName, model)

    // Delete old table and create new one
    dbModel.sync()
    .then(() => {
        console.log("[PG] The table for '" + modelName + "' successfully created")
    })

    return dbModel
}