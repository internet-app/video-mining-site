const http = require('http')

const getSiteIpInfo = (ipAddress) => {
    const options = {
        hostname: 'ip-api.com',
        port: 80,
        path: `/json/${ipAddress}`,
    }

    return new Promise((resolve, reject) => {
        const request = http.get(options, (response) => {
            // handle http errors
            if (response.statusCode < 200 || response.statusCode > 299) {
                reject(new Error('Failed to load page, status code: ' + response.statusCode));
            }
            // temporary data holder
            const dataBuf = [];
            // on every content chunk, push it to the data array
            response.on('data', (chunk) => dataBuf.push(chunk));
            // we are done, resolve promise with those joined chunks
            response.on('end', () => resolve(dataBuf.join('')));
        })

        request.on('error', (err) => reject(err))
    })
}

const getIpInfo = async (ipAddress) => {
    const ip = ipAddress.substring(7)
    const ipInfo = await getSiteIpInfo(ip)
    const ipObj = JSON.parse(ipInfo)

    return ipObj
}

module.exports = {
    getIpInfo
}
