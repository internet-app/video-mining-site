// Run script from the 'app' folder only
const { exec } = require('child_process')
const fs = require('fs')
const { join } = require('path')

const dirs = (path) => fs.readdirSync(path).filter(
    (obj) => fs.statSync(join(path, obj)).isDirectory()
)

const createUniqueDir = () => {
    const projectPath = `${__dirname}/..`
    
    // Find free index to store stream
    const dirList = dirs(`${projectPath}/public/stream/`)
    let i = 0
    while(dirList.indexOf( i.toString() ) !== -1) {
        i += 1
    }
    fs.mkdirSync(`${projectPath}/public/stream/${i}`)

    return `stream/${i}`
}

const create = (videoPath, streamDir) => {
    const projectPath = `${__dirname}/..`

    // Make basic checks
    if (videoPath === 0) {
        console.log('Pass the path of a video file')
        process.exit(0)
    }
        
    if (!fs.existsSync(videoPath)) {
        console.log('The video file does not exist')
        process.exit(0)
    }
    
    const cmdFFmpegRunFor = `ffmpeg -hide_banner -y -i "${videoPath}"`
    const cmdEncode360p = `-vf scale=w=640:h=360:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename "${projectPath}/public/${streamDir}/360p_%03d.ts" "${projectPath}/public/${streamDir}/360p.m3u8"`
    // const cmdEncode480p = '-vf scale=w=842:h=480:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename "${projectPath}/public/${streamDir}/480p_%03d.ts" "${projectPath}/public/${streamDir}/480p.m3u8"'
    // const cmdEncode720p = '-vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename "${projectPath}/public/${streamDir}/720p_%03d.ts" "${projectPath}/public/${streamDir}/720p.m3u8"'
    const cmdEncode1080p = `-vf scale=w=1920:h=1080:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename "${projectPath}/public/${streamDir}/1080p_%03d.ts" "${projectPath}/public/${streamDir}/1080p.m3u8"`

    const resultCmd = `${cmdFFmpegRunFor} ${cmdEncode360p} ${cmdEncode1080p}`
    
    // const playlist = `#EXTM3U
    // #EXT-X-VERSION:3
    // #EXT-X-STREAM-INF:BANDWIDTH=800000,RESOLUTION=640x360
    // 360p.m3u8
    // #EXT-X-STREAM-INF:BANDWIDTH=1400000,RESOLUTION=842x480
    // 480p.m3u8
    // #EXT-X-STREAM-INF:BANDWIDTH=2800000,RESOLUTION=1280x720
    // 720p.m3u8
    // #EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
    // 1080p.m3u8`
    
    const playlist = `#EXTM3U
    #EXT-X-VERSION:3
    #EXT-X-STREAM-INF:BANDWIDTH=800000,RESOLUTION=640x360
    360p.m3u8
    #EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
    1080p.m3u8`
    
    return new Promise((resolve, reject) => {
        exec(resultCmd, (err, stdout, stdin) => {
            if (err !== null)
            {
                return reject(err)
            }
        
            fs.writeFile(`${projectPath}/public/${streamDir}/playlist.m3u8`, playlist, (err) => {
                if(err) {
                    return reject(err)
                }
        
                // Remove video
                fs.unlink(videoPath, (err) => {
                    if (err) {
                        return reject(err)
                    }
                })
    
                resolve('Done')
            })
        })
    })
}

module.exports = {
    create,
    createUniqueDir
}
