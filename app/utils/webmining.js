const https = require('https')
const coinimp = require('../config/coinimp')

const getUserInfo = (username) => {
    const options = {
        hostname: 'www.coinimp.com',
        port: 443,
        path: `${coinimp.URL_USER_BALANCE}?site-key=${coinimp.SITE_KEY}&user=${username}`,
        headers: {
            'X-API-ID': coinimp.PUBLIC_KEY,
            'X-API-KEY': coinimp.PRIVATE_KEY
        }
    }

    return new Promise((resolve, reject) => {
        const request = https.get(options, (response) => {
            // handle http errors
            if (response.statusCode < 200 || response.statusCode > 299) {
                reject(new Error('Failed to load page, status code: ' + response.statusCode));
            }
            // temporary data holder
            const dataBuf = [];
            // on every content chunk, push it to the data array
            response.on('data', (chunk) => dataBuf.push(chunk));
            // we are done, resolve promise with those joined chunks
            response.on('end', () => resolve(dataBuf.join('')));
        })

        request.on('error', (err) => reject(err))
    })
}

const getUserCountHashes = async (username) => {
    const userInfo = await getUserInfo(username)
    const userObj = JSON.parse(userInfo)

    if (userObj.status === 'failure') {
        return null
    } else {
        return userObj.message.hashes
    }
}

module.exports = {
    getUserCountHashes
}
