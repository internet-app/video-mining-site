const initPlayer = () => {
    const videoElement = document.getElementsByClassName('video-stream')[0]
    const videoLink = `${window.location.origin}/stream/${window.videoId}/playlist.m3u8`
    if(Hls.isSupported()) {
        var hls = new Hls();
        hls.loadSource(videoLink);
        hls.attachMedia(videoElement);
    } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
        videoElement.src = videoLink;
    }
}

document.addEventListener('DOMContentLoaded', initPlayer)
