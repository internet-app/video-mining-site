// Run script from the 'app' folder only
const { exec } = require('child_process');
const fs = require('fs')
const { join } = require('path')

// Remove first two arguments
const args = process.argv.slice(2)

const dirs = (path) => fs.readdirSync(path).filter(
    (obj) => fs.statSync(join(path, obj)).isDirectory()
)

// Make basic checks
if (args.length === 0) {
    console.log('Pass the path of a video file')
    process.exit(0)
}

if (!fs.existsSync(args[0])) {
    console.log('The video file does not exist')
    process.exit(0)
}

// Find free index to store stream
const dirList = dirs('public/stream/')
let i = 0
while(dirList.indexOf( i.toString() ) !== -1) {
    i += 1
}
fs.mkdirSync(`public/stream/${i}`)

const cmdFFmpegRunFor = `ffmpeg -hide_banner -y -i "${args[0]}"`
const cmdEncode360p = `-vf scale=w=640:h=360:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename "public/stream/${i}/360p_%03d.ts" "public/stream/${i}/360p.m3u8"`
// const cmdEncode480p = '-vf scale=w=842:h=480:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename "public/stream/${i}/480p_%03d.ts" "public/stream/${i}/480p.m3u8"'
// const cmdEncode720p = '-vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename "public/stream/${i}/720p_%03d.ts" "public/stream/${i}/720p.m3u8"'
const cmdEncode1080p = `-vf scale=w=1920:h=1080:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename "public/stream/${i}/1080p_%03d.ts" "public/stream/${i}/1080p.m3u8"`

const resultCmd = `${cmdFFmpegRunFor} ${cmdEncode360p} ${cmdEncode1080p}`

// const playlist = `#EXTM3U
// #EXT-X-VERSION:3
// #EXT-X-STREAM-INF:BANDWIDTH=800000,RESOLUTION=640x360
// 360p.m3u8
// #EXT-X-STREAM-INF:BANDWIDTH=1400000,RESOLUTION=842x480
// 480p.m3u8
// #EXT-X-STREAM-INF:BANDWIDTH=2800000,RESOLUTION=1280x720
// 720p.m3u8
// #EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
// 1080p.m3u8`

const playlist = `#EXTM3U
#EXT-X-VERSION:3
#EXT-X-STREAM-INF:BANDWIDTH=800000,RESOLUTION=640x360
360p.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
1080p.m3u8`

exec(resultCmd, (err, stdout, stdin) => {
    if (err !== null)
    {
        console.log(err)
        return
    }

    fs.writeFile(`public/stream/${i}/playlist.m3u8`, playlist, (err) => {
        if(err) {
            console.log(err);
            return
        }

        console.log("Done");
    }); 
})
