const POSTGRES = {
    HOST: "127.0.0.1",
    PORT: 5432,
    USER: "root",
    PASSWORD: "mysecretpassword",
    DB: "test"
}

module.exports = POSTGRES
