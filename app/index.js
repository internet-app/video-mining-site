const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const cookieParser = require('cookie-parser')
const cryptoRandomString = require('crypto-random-string')
const crypto = require('crypto')
const ipApi = require('./utils/ip_api')

// Bring in User Model
const ActiveUser = require('./db/models/active-user')

// Init app
const app = express()

// Load view engine
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(expressValidator())
app.use(cookieParser())

// Check and set cookie
app.use(async(req, res, next) => {
    next()
    return
    if (req.cookies['uid'] === undefined) {
        const uid = cryptoRandomString({length: 64, type: 'base64'})
        
        if (req.headers['user-agent'] === undefined || req.headers['accept-language'] === undefined)
        {
            return res.sendStatus(400)
        }
        const userAgent = crypto.createHash('sha1')
            .update(req.headers['user-agent']).digest('hex')
        const acceptLanguage = crypto.createHash('sha1')
            .update(req.headers['accept-language']).digest('hex')
        const ipInfo = await ipApi.getIpInfo(req.connection.remoteAddress)

        let country = null
        let city = null

        if (ipInfo.status === 'success') {
            country = ipInfo.country
            city = ipInfo.city
        }

        await ActiveUser.create({ 
            uid: uid,
            user_id: 0,
            user_agent: userAgent,
            remote_address: req.connection.remoteAddress,
            accept_language: acceptLanguage,
            country: country,
            city: city
        })
        res.cookie('uid', uid, { maxAge: 900000, httpOnly: true })
        
        next()
    } else {
        const activeUserEntry = await ActiveUser.findOne({ 
            where: {
                uid: req.cookies['uid']
            }
        }).catch((err) => console.log(err))

        // If guest user
        if (activeUserEntry.user_id === 0) {
            next()
            return
        }

        const sha1agent = crypto.createHash('sha1').update(req.headers['user-agent'])
        if (sha1agent.digest('hex') !== activeUserEntry.user_agent) {
            res.send("Access forbidden due to agent")
        }

        if (activeUserEntry.remote_address !== req.connection.remoteAddress) {
            const ipInfo = ipApi.getIpInfo(req.connection.remoteAddress)
            if (ipInfo.status === 'success' 
                && activeUserEntry.city !== null 
                && activeUserEntry.country !== null) {
                if (activeUserEntry.city !== ipInfo.city || activeUserEntry.country !== ipInfo.country) {
                    res.send("Access forbidden due to ip")
                }
            }
        }

        const sha1language = crypto.createHash('sha1').update(req.headers['accept-language'])
        if (sha1language.digest('hex') !== activeUserEntry.accept_language) {
            res.send("Access forbidden due to language")
        }

        next()
    }
})

// Body Parser Middleware
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// Define basic routers
app.use('/',            require('./routes/home'))
app.use('/user/',       require('./routes/user'))
app.use('/api/',        require('./routes/api'))
app.use('/profile/',    require('./routes/profile'))
app.use('/video/',      require('./routes/video'))
app.use('/mobile_api/', require('./routes/mobile_api'))

app.use(express.static('public'))

// Start Server
const listener = app.listen(3000, () => {
    console.log(`Example app listening on port ${listener.address().port}!`)
})
