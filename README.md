# video-mining-site
That is the repository of Video Mining Project. That project helps people to view new films almost free.
People who want to view film need to give us resources of his computer. 
If the user's computer is powerful then it can start view film much faster than the user with low computer resources.

## 1. Requirements
To run this site you need to install Docker and NodeJs.

You can install Docker by the following [link](https://docs.docker.com/install/).
Also you might need to install posgres docker container by execution the following command:
```bash
docker pull postgres
```
!Note: you may need root rights

You can install NodeJs by the following [link](https://nodejs.org/en/download/).

## 2. Install
Clone the repository or download and unpack it.
Go to the *video-mining-site/app/* folder.
To install all dependencies you need to run the following command:
```bash
npm install
```

## 3. Run
To run the database fot this site you need kill all running docker containers and run the following command:
```bash
npm run init-docker
```
!Note: you may need root rights

After that you need to wait about three seconds.
Run this site by exece the following command:
```bash
nodemon
```

## 4. Add new video stream
To add new video stream you need to execute the folliwing command in 'app' folder:
```bash
npm run add-stream -- path/to/stream.mp4
```

After executing the command above, in the directory *video-mining-site/app/public/stream/* appear new folder with index.
To watch stream you need to change *video-mining-site/app/views/index.pug* file:
```pug
const videoLink = window.location.origin + '/stream/0/playlist.m3u8'
```
And change 0 to the new index.

## 5. Configure VSCode for work with NodeJS
1. Install NodeJS on Ubuntu. To do that, you need to execute:
```bash
sudo apt-get install curl python-software-properties
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install nodejs
```
2. Install ESLint by excute the following commands:
```bash
npm install eslint
```
3. In VSCode you need to press `Ctrl+P` and set `ext install waderyan.nodejs-extension-pack`

## Further development
1. Create authentication:
    1. DB (Postgres):
        1. USERS:
            - id(8 bytes)
            - name (up to 200 char)
            - password (hash sha256: 32 bytes)
        2. ACTIVE_USERS:
            - users_id(8 bytes)
            - session(32 bytes)
    2. REST/GraphQL service
    3. HTML page/form
    4. Cookie

## Programming standart
https://standardjs.com/rules.html

## Check PostgreSQL:
1. Connect to database:
```bash
docker exec -it some-postgres psql test root
```
2. View existing tables:
```psql
\d
```
3. View current table coulmns:
```psql
\d <table name>
```
4. View table content:
```sql
select * from <table name>
```

## How bcrypt works?
- 2a - bcrypt identifier
- 10 - 2^10 iteration count
- 22 characters - salt (Base64; 128 bits)
- 31 characters - ciphertext (Base64; 184 bits)

### Example:
```
$2b$10$RS5EARiPuL44Ual95WRtseW3BY.fP.rqgiS9dDSxIXZHbl6rD7pI.
$2b$10$3euPcmQFCiblsZeEu5s7p.9OVHgeHWFDk9nhMqZ0m/3pd/lhwZgES
```
