# Description of implementation content security policy (CSP)

## Definition

**Content Security Policy** - a computer security standard introduced to prevent cross-site scripting (XSS), clickjacking (fake web page layer) and other code injection attacks resulting from execution of malicious content in the trusted web page context.
So we can say to the browser, which content must be executed.

## Threats
### **Mitigating cross site scripting**
CSP makes it possible for server administrators to reduce or eliminate the vectors by which XSS can occur by specifying the domains that the browser should consider to be valid sources of executable scripts. A CSP compatible browser will then only execute scripts loaded in source files received from those allowlisted domains, ignoring all other script (including inline scripts and event-handling HTML attributes).

### **Mitigating packet sniffing attacks**
The server can specify which protocols are allowed to be used; for example (and ideally, from a security standpoint), a server can specify that all content must be loaded using HTTPS. A complete data transmission security strategy includes not only enforcing HTTPS for data transfer, but also marking all cookies with the secure flag and providing automatic redirects from HTTP pages to their HTTPS counterparts. Sites may also use the Strict-Transport-Security HTTP header to ensure that browsers connect to them only over an encrypted channel.

## Example of configuration
A web site administrator wants to allow content from a trusted domain and all its subdomains (it doesn't have to be the same domain that the CSP is set on.)
```
Content-Security-Policy: default-src 'self' *.trusted.com
```
A web site administrator wants to allow users of a web application to include images from any origin in their own content, but to restrict audio or video media to trusted providers, and all scripts only to a specific server that hosts trusted code.
```
Content-Security-Policy: default-src 'self'; img-src *; media-src media1.com media2.com; script-src userscripts.example.com
```

## How to say browser that content does not executable
To enable CSP, you need to configure your web server to return the Content-Security-Policy HTTP header.
Alternatively, the <meta> element can be used to configure a policy, for example: <meta http-equiv="Content-Security-Policy" content="default-src 'self'; img-src https://*; child-src 'none';">

Example HTTP header with content security policy field:
```html
HTTP/1.1 200 OK
X-Powered-By: Express
Set-Cookie: uid=D6OpwWYr%2Bb5%2Fx6FrXYD9SOHd9LJPWdTBkCx5YEcSvLMPl9%2FsJX505SXS8ta25gRW; Max-Age=900; Path=/; Expires=Mon, 27 May 2019 15:21:48 GMT; HttpOnly
Content-Security-Policy: default-src 'self'; script-src 'nonce-wj1opr7N69zLCTKfkF0FZg=='; worker-src blob:; media-src blob:;
Content-Type: text/html; charset=utf-8
Content-Length: 957
ETag: W/"3bd-0E8rLqzoCBRSMIpEUu8Wv7Izyz0"
Date: Mon, 27 May 2019 15:06:48 GMT
Connection: keep-alive
```

## Testing
To ease deployment, CSP can be deployed in report-only mode. The policy is not enforced, but any violations are reported to a provided URI. Additionally, a report-only header can be used to test a future revision to a policy without actually deploying it.
```
Content-Security-Policy-Report-Only: policy
```
