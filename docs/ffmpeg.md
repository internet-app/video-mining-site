# This folder used for FFmpeg customization to create video-stream

1. Show properties of the video:
```bash
ffprobe -hide_banner videoplayback.mp4
```
Example output:
```
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'videoplayback.mp4':
  Metadata:
    major_brand     : mp42
    minor_version   : 0
    compatible_brands: isommp42
    creation_time   : 2018-10-08T14:50:03.000000Z
  Duration: 00:04:41.57, start: 0.000000, bitrate: 733 kb/s
    Stream #0:0(und): Video: h264 (Constrained Baseline) (avc1 / 0x31637661), yuv420p(tv, bt709), 640x360 [SAR 1:1 DAR 16:9], 634 kb/s, 23.98 fps, 23.98 tbr, 24k tbn, 47.95 tbc (default)
    Metadata:
      creation_time   : 2018-10-08T14:50:03.000000Z
      handler_name    : ISO Media file produced by Google Inc. Created on: 10/08/2018.
    Stream #0:1(und): Audio: aac (LC) (mp4a / 0x6134706D), 44100 Hz, stereo, fltp, 95 kb/s (default)
    Metadata:
      creation_time   : 2018-10-08T14:50:03.000000Z
      handler_name    : ISO Media file produced by Google Inc. Created on: 10/08/2018.
```

2. Build sample hls video stream:
```bash
ffmpeg -i videoplayback.mp4 \
-vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -b:a 128k -c:v h264 -profile:v main -crf 20 -g 48 -keyint_min 48 -sc_threshold 0 -b:v 2500k -maxrate 2675k -bufsize 3750k -hls_time 4 -hls_playlist_type vod -hls_segment_filename beach/720p_%03d.ts 2/manifest720p.m3u8
```

Another example
```bash
ffmpeg -hide_banner -y -i beach.mkv \
  -vf scale=w=640:h=360:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename beach/360p_%03d.ts beach/360p.m3u8 \
  -vf scale=w=842:h=480:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename beach/480p_%03d.ts beach/480p.m3u8 \
  -vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename beach/720p_%03d.ts beach/720p.m3u8 \
  -vf scale=w=1920:h=1080:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename beach/1080p_%03d.ts beach/1080p.m3u8
```

It has the following options:
- **-i beach.mkv** - set *beach.mkv* as input file
- **-vf "scale=w=1280:h=720:force_original_aspect_ratio=decrease"** - scale video to maximum possible within *1280x720 while preserving aspect ratio*
- **-c:a aac -ar 48000 -b:a 128k** - set audio codec to AAC with sampling of 48kHz and bitrate of 128k
- **-c:v h264** - set video codec to be *H264* which is the *standard codec of HLS* segments
- **-profile:v main** - set H264 profile to main - this means support in modern devices (http://blog.mediacoderhq.com/h264-profiles-and-levels/)
- **-crf 23** - Constant Rate Factor, high level factor for overall quality (from 0 - high quality to 51 - bad quality. Often used 23). It is the rate of the compression by libx264 codec.
- **-g 48 -keyint_min 48** - IMPORTANT create key frame (I-frame) every 48 frames (~2 seconds) - will later affect correct slicing of segments and alignment of renditions. Defines diapasons of of frames per chunk. (-g <kf-max-dist> -keyint_min <kf-min-dist>)
- **-sc_threshold 0** - don't create key frames on scene change - only according to -g (be default for HLS must be equals 0)
- **-b:v 2500k -maxrate 2675k -bufsize 3750k** - limit video bitrate, these are rendition specific and depends on your content type (https://docs.peer5.com/guides/production-ready-hls-vod/#how-to-choose-the-right-bitrate)
- **-hls_time 4** - segment target duration in seconds - the actual length is constrained by key frames
- **-hls_playlist_type vod** - adds the #EXT-X-PLAYLIST-TYPE:VOD tag and keeps all segments in the playlist
- **-hls_segment_filename beach/720p_%03d.ts** - explicitly define segments files names
- **beach/manifest720p.m3u8** - path of the playlist file - also tells ffmpeg to output HLS (.m3u8)
