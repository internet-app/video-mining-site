# Nginx

## 1. Download Docker container
Use the following command to pull Docker conatiner with Nginx
```bash
docker pull nginx
```

## 2. Create root sertificate
### 2.1. Create secret key
The following command create *rootCA.key*.
```bash
openssl genrsa -out rootCA.key 2048
```
### 2.2. Create sertificate
```bash
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem
```

## 3. Create self-signed sertificate
### 3.1. Create certificate signing request
```bash
openssl req -new -newkey rsa:2048 -sha256 -nodes -keyout device.key -out device.csr
```
### 3.2. Create certificate file
```bash
echo -e "authorityKeyIdentifier=keyid,issuer\n\
basicConstraints=CA:FALSE\n\
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment\n\
subjectAltName = @alt_names\n\
\n\
[alt_names]\n\
DNS.1 = %%DOMAIN%%\n\
DNS.2 = *.%%DOMAIN%%" > v3.ext
```
### 3.3. Create temporary file
```bash
cat v3.ext | sed s/%%DOMAIN%%/mysite.localhost/g > __v3.ext
```
### 3.4. Create sertificate for 30 days
```bash
openssl x509 -req -in device.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out device.crt -days 30 -sha256 -extfile __v3.ext
```
### 3.5. Change name and remove file
```bash
mv device.csr mysite.localhost.csr
mv device.crt mysite.localhost.crt
```
### 3.6. View sertificate information
```bash
openssl x509 -in cerfile.cer -noout -text
```

### 3.7. Nginx configuration
```nginx.conf
user  nginx; # Defines user and group credentials used by worker processes
worker_processes  auto;
events { } # Contains directives that affect connection processing

error_log  /var/log/nginx/error.log warn; # Configures logging [path] [level]
pid        /var/run/nginx.pid; # Store the PID of the main process

http { # Defines the http server directives
    server_tokens off; # Disable emitting nginx version on error pages and in the “Server” response header field
    limit_conn_zone $binary_remote_addr zone=addr:5m; # Sets parameters for a shared memory zone that will keep states for various keys. In particular, the state includes the current number of connections.

    server { # Sets configuration for a virtual server
        if ($request_method !~ ^(GET|POST)$ ) {
		    return 444;
	    }

        listen 80; # Handle all connections from 80 port
        return 301 https://127.0.0.1:8443$request_uri; # Send "Moved Permanently" error code and redirect
    }

    server {
        if ($request_method !~ ^(GET|POST)$ ) {
		    return 444;
	    }

        listen 443 ssl; # Handle all connections from 443 port. Work in SSL mode

        ssl_certificate /etc/ssl/certs/certificate_bundled.crt;
        ssl_certificate_key /etc/ssl/device.key;
        ssl_session_cache shared:SSL:10m;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;

        # "$binary_remote_addr" client address in a binary form, value’s length is always 4 bytes for IPv4 addresses or 16 bytes for IPv6 addresses

        location / { # configuration depending on a request URI

            proxy_pass http://172.17.0.1:3000; # protocol and address to which a location should be mapped. Can be bunch of locations
            proxy_set_header Host $host; # Allows redefining or appending fields to the request header passed to the proxied server

            proxy_redirect http:// https://; # re-write redirects to http as to https
        }
    }
}
```

### 3.8. Run Nginx Docker container
```bash
docker run -d --rm --name my-custom-nginx-container \
    -v $(pwd)/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v $(pwd)/certificate_bundled.crt:/etc/ssl/certs/certificate_bundled.crt:ro \
    -v $(pwd)/device.key:/etc/ssl/device.key:ro -p 8443:443 nginx
```
